It doesn't seem to make a difference redirecting with the `_path` helper

    redirect_to urls_path

or with the `_url` helper

    redirect_to urls_url

I guess Rails handles the "always redirect to a complete URL" rule for us.

### Testing

After setup and `rails s -p 3333`

    $ curl -i localhost:3333/paths

outputs

```
HTTP/1.1 302 Found
X-Frame-Options: SAMEORIGIN
X-XSS-Protection: 1; mode=block
X-Content-Type-Options: nosniff
X-Download-Options: noopen
X-Permitted-Cross-Domain-Policies: none
Referrer-Policy: strict-origin-when-cross-origin
Location: http://localhost:3333/urls
Content-Type: text/html; charset=utf-8
Cache-Control: no-cache
X-Request-Id: 7b12b93f-8a96-41de-bfb9-4b31bdda6851
X-Runtime: 0.003058
Transfer-Encoding: chunked

<html><body>You are being <a href="http://localhost:3333/urls">redirected</a>.</body></html>
```

while

    $ curl -i localhost:3333/urls

outputs

```
HTTP/1.1 302 Found
X-Frame-Options: SAMEORIGIN
X-XSS-Protection: 1; mode=block
X-Content-Type-Options: nosniff
X-Download-Options: noopen
X-Permitted-Cross-Domain-Policies: none
Referrer-Policy: strict-origin-when-cross-origin
Location: http://localhost:3333/paths
Content-Type: text/html; charset=utf-8
Cache-Control: no-cache
X-Request-Id: d76d7fec-ac34-4a4d-8dd6-15dc0fda3b1b
X-Runtime: 0.002564
Transfer-Encoding: chunked

<html><body>You are being <a href="http://localhost:3333/paths">redirected</a>.</body></html>
```
