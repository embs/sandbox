Rails.application.routes.draw do
  resources :paths, only: :index
  resources :urls, only: :index
end
